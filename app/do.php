<?php
use Slim\Factory\AppFactory;

require_once 'config.php';

// Slim initialization
$app = AppFactory::create();
$app->get('/', '\Idol\Controllers\IdolController:mGETaMain');
$app->get('/groups', '\Idol\Controllers\IdolController:mGETaGroups');
$app->get('/groups/{group}', '\Idol\Controllers\IdolController:mGETaInfo');
$app->get('/groups/{group}/members', '\Idol\Controllers\IdolController:mGETaMembers');
$app->get('/groups/{group}/members/{member}', '\Idol\Controllers\IdolController:mGETaMember');
$app->get('/groups/{group}/singles', '\Idol\Controllers\IdolController:mGETaSingles');
$app->get('/groups/{group}/singles/{num}', '\Idol\Controllers\IdolController:mGETaSingle');

$app->run();
