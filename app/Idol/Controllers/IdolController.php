<?php
namespace Idol\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class IdolController {

  private $twig;

  public function __construct() {
    // Twig initialization
    $loader = new \Twig\Loader\FilesystemLoader(TEMPLATES_DIR);
    $this->twig = new \Twig\Environment($loader);
  }

  public function mGETaMain(Request $request, Response $response, array $args) {
    $json=json_decode(file_get_contents("../app/data/groups.json"));
    $data=[
      'data'=>$json->groups
    ];
    $html = $this->twig->render('main.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaGroups(Request $request, Response $response, array $args) {
    $json=json_decode(file_get_contents("../app/data/groups.json"));
    $data=[
        'data'=>$json->groups
    ];
    $html = $this->twig->render('groups.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaInfo(Request $request, Response $response, array $args) {
    $group=$args["group"];
    $json=json_decode(file_get_contents("../app/data/groups/$group.json"));
    $data=[
        'group'=>$json
    ];
    $html = $this->twig->render('group_info.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaMembers(Request $request, Response $response, array $args) {
    $group=$args["group"];
    $json=json_decode(file_get_contents("../app/data/groups/$group.json"));
    $data=[
        "data"=>$json->members,
        "group"=>$group,
        "groupnormal"=>$json->name->normal
    ];
    $html = $this->twig->render('members.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaMember(Request $request, Response $response, array $args) {
    $group=$args["group"];
    $json=json_decode(file_get_contents("../app/data/groups/$group.json"));
    $name=explode("_",$args["member"]);
        foreach($json->members as $mem){
            if($mem->name->r==$name[1]&&$mem->surname->r==$name[0]){
                $result=$mem;
            }
        }
        if(!isset($result))_die();
        $data=[
            "member"=>$result,
            "group"=>$group,
            "groupnormal"=>$json->name->normal
        ];
    $html = $this->twig->render('member.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaSingles(Request $request, Response $response, array $args) {
    $group=$args["group"];
    $json=json_decode(file_get_contents("../app/data/groups/$group.json"));
    $data=[
        "data"=>$json->discography,
        "group"=>$group,
        "groupnormal"=>$json->name->normal
    ];
    $html = $this->twig->render('singles.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaSingle(Request $request, Response $response, array $args) {
    $group=$args["group"];
    $single=$args["num"];
    $json=json_decode(file_get_contents("../app/data/groups/$group.json"));
    foreach($json->discography as $s){
      if($s->num==$single){
          $result=$s;
      }
    }
    if(!isset($result))_die();
    $data=[
        "data"=>$result,
        "group"=>$group,
        "groupnormal"=>$json->name->normal
    ];
    $html = $this->twig->render('single.twig',$data);
    $response->getBody()->write($html);
    return $response;
  }

};